import StyleVarSettingsHandler from "./StyleVarSettingsHandler.js";

// register a11y colors
new StyleVarSettingsHandler("location_status_opened_color", "world-entry-status-opened-color");
new StyleVarSettingsHandler("location_status_available_color", "world-entry-status-available-color");
new StyleVarSettingsHandler("location_status_unavailable_color", "world-entry-status-unavailable-color");
new StyleVarSettingsHandler("location_status_possible_color", "world-entry-status-possible-color");
